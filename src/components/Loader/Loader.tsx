import { FC } from "react";
import styles from "./Loader.module.scss"

type Props = {
    center?: boolean
}
const Loader: FC<Props> = ({center}: Props) => {
    let style: string = styles.loader
    if (center) {
        style += (" " + styles.loader__center)
    }

    return (
        <div className={style}></div>
    );
};

export default Loader