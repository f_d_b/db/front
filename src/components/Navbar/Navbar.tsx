'use client'
// FIXME: хз почему, но getServerSession не выдает сессию, из-за чего приходится делать use client

import Link from 'next/link'
import Logo from '@/components/Logo/Logo'
import styles from './Navbar.module.scss'
import { getServerSession } from 'next-auth'
import { authConfig } from '@/configs/auth'
import Button from '@/components/Button/Button'
import {signOut, useSession} from 'next-auth/react'
import {redirect} from "next/navigation";

const Navbar = () => {
	const session: any = useSession()
	if (!session) {
		redirect('/login')
	}

	return (
		<nav className={styles.nav}>
			<Logo />
			<ul>
				<li>
					<Link href="/">Главная</Link>
				</li>
				<li>
					<Link href="/defects">Маршруты</Link>
				</li>
				<li>
					<Link href="/photo">Фотодетекция</Link>
				</li>
				{/*<li>*/}
				{/*	<Link href="/help">Помощь</Link>*/}
				{/*</li>*/}
			</ul>

			{session?.status === 'authenticated' && (
				<Button style={{ marginTop: '20px' }} full onClick={() => signOut()}>
					Выход
				</Button>
			)}
		</nav>
	)
}

export default Navbar
