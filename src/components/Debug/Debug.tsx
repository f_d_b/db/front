'use client'
import { signOut, useSession } from 'next-auth/react'
import Link from 'next/link'
import Button from '@/components/Button/Button'

const Debug = () => {
	const session: any = useSession()
	return (
		<div className="debug-bar">
			<Button style={{ right: 0 }}>
				<Link href="#" onClick={() => signOut({ callbackUrl: '/' })}>
					Sign Out
				</Link>
			</Button>
			<pre style={{ color: '#fff', padding: '5px'}}>
				{/*data: {session.data}*/}
				<br />status: {session.status}
			</pre>
		</div>
	)
}

export default Debug
