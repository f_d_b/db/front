'use client'

import { SessionProvider } from 'next-auth/react'
import { useState } from 'react'
import RefreshTokenHandler from "@/utils/RefreshTokenHandler";

export const Providers = ({ children }: { children: React.ReactNode }) => {
	const [interval, setInterval] = useState(0)

	return (
		<SessionProvider>
			{children}
			<RefreshTokenHandler setInterval={setInterval} />
		</SessionProvider>
	)
}
