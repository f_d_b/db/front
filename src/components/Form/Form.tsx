'use client'

import Button from '@/components/Button/Button'
import {FC, Fragment, SyntheticEvent} from 'react'
import styles from './Form.module.scss'

interface InputInterface {
	label: string
	type: string
	name: string
	placeholder?: string
}

interface Props {
	inputs: InputInterface[]
	submitHandler: (e: SyntheticEvent<HTMLFormElement>) => void
	buttonText: string
	title?: string
}

const Form: FC<Props> = ({ inputs, submitHandler, buttonText, title }) => {
	return (
		<form className={styles.form} onSubmit={submitHandler}>
			<legend>{title}</legend>
			{inputs.map((input) => {
				return (
					<Fragment key={input.name}>
						<label htmlFor={input.name}>{input.label}</label>
						<input
							type={input.type}
							name={input.name}
							placeholder={input?.placeholder}
						/>
					</Fragment>
				)
			})}

			<Button>{buttonText}</Button>
		</form>
	)
}

export default Form
