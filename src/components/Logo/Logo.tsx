import Image from "next/image";
import logo from 'pub/Logo.png'
const Logo = () => {
    return <Image src={logo} alt={'Логотип'}/>
}

export default Logo