import { FC, ReactNode } from 'react'
import styles from './Text.module.scss'

interface Props {
    children: ReactNode
}

const Text: FC<Props> = ({ children }) => {
	return <p className={styles.text}>{children}</p>
}

export default Text