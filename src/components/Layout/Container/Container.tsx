import { FC, ReactNode } from 'react'
import styles from './Container.module.scss'

interface Props {
	children: ReactNode,
	full?: boolean
	mode?: 'v'|'h'
}

const Container: FC<Props> = ({ children, mode = 'v', full = false}) => {

	let classNames = [styles.container]

	switch (mode) {
		case 'v':
			classNames.push(styles.container_v)
			break
		case 'h':
			classNames.push(styles.container_h)
			break
	}

	if (full) {
		classNames.push(styles.full)
	}

	return <div className={classNames.join(' ')}>{children}</div>
}

export default Container
