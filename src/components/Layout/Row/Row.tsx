import { FC, ReactNode } from 'react'
import styles from './Row.module.scss'

type Props = {
	children: ReactNode
	column?: boolean
	wrap?: boolean
	fill?: boolean
	offsetTop?: boolean
	vertical?: string
}

const Row: FC<Props> = ({
	children,
	column,
	wrap,
	fill,
	offsetTop,
	vertical,
}: Props) => {
	let classNames = [styles.row]
	if (column) {
		classNames.push(styles.row__column)
	}
	if (wrap) {
		classNames.push(styles.row__wrap)
	}
	if (fill) {
		classNames.push(styles.row__fill)
	}
	if (offsetTop) {
		classNames.push(styles.row__offsetTop)
	}

	switch (vertical) {
		case 'top':
			classNames.push(styles.row__verticalTop)
			break
		case 'center':
			classNames.push(styles.row__verticalCenter)
			break
		case 'bottom':
			classNames.push(styles.row__verticalBottom)
			break
		default:
			break
	}

	return <div className={classNames.join(' ')}>{children}</div>
}

export default Row