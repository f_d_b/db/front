import { FC, ReactNode } from 'react'
import styles from './Title.module.scss'

interface Props {
	children: ReactNode
	h?: 1 | 2 | 3 | 4 | 5 | 6
}

const Title: FC<Props> = ({ children, h = 1 }) => {
	switch (h) {
		case 1:
			return <h1 className={styles.h1}>{children}</h1>
		case 2:
			return <h1 className={styles.h2}>{children}</h1>
		case 3:
			return <h1 className={styles.h3}>{children}</h1>
		case 4:
			return <h1 className={styles.h4}>{children}</h1>
		case 5:
			return <h1 className={styles.h5}>{children}</h1>
		case 6:
			return <h1 className={styles.h6}>{children}</h1>
		default:
			return <h1 className={styles.h1}>{children}</h1>
	}
}

export default Title
