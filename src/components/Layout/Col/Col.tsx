import { FC, ReactNode } from 'react'
import styles from './Col.module.scss'

type Props = {
	children: ReactNode
	grow?: number
	width?: number
}

const Col: FC<Props> = ({ children, grow, width }: Props) => {
	return (
		<div
			className={styles.col}
			style={{
				flexGrow: grow,
				width: `${width}px`,
			}}>
			{children}
		</div>
	)
}

export default Col
