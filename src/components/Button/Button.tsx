import {CSSProperties, FC, ReactNode, SyntheticEvent} from 'react'
import styles from './Button.module.scss'

interface Props {
	children: ReactNode
	style?: CSSProperties
	full?: boolean // отображать во всю ширину
	onClick?: (event: SyntheticEvent) => void
	disabled?: boolean
}

const Button: FC<Props> = ({ children, style, full, disabled }) => {
	let classNames = [styles.btn]
	if (full) {
		classNames.push(styles.full)
	}

	return (
		<button className={classNames.join(' ')} style={style} disabled={disabled}>
			{children}
		</button>
	)
}

export default Button
