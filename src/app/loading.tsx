import Loader from "@/components/Loader/Loader";

const Loading = () => {
	return <Loader center></Loader>
}

export default Loading
