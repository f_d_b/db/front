'use client'
import Form from '@/components/Form/Form'
import {FormEvent, SyntheticEvent} from "react";
import {signIn, useSession} from "next-auth/react";

const RegisterPage = () => {
    const session = useSession()
    const submitHandler = async (e: SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()

        const data: FormData = new FormData(e.currentTarget);
        const payload = {
            email: data.get('email'),
            password: data.get('password'),
            password_dub: data.get('password_dub')
        }

        const res = await signIn('credentials', payload)

        console.log('submit')
    }

    return (
        <>
            <h1>Login page</h1>

            {session.data ? (
                <h6>You just login in</h6>
            ) : (
                <Form
                    inputs={[
                        {
                            label: 'email',
                            name: 'email',
                            type: 'text',
                            placeholder: 'admin@mail.ru',
                        },
                        {
                            label: 'password',
                            name: 'password',
                            type: 'password',
                        },
                        {
                            label: 'duplicate password',
                            name: 'password_dub',
                            type: 'password',
                        },
                    ]}
                    submitHandler={submitHandler}
                    title="Вход"
                    buttonText='Войти'
                />
            )}
        </>
    )
}

export default RegisterPage
