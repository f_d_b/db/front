'use client'
import Form from '@/components/Form/Form'
import {SyntheticEvent, useEffect} from 'react'
import { signIn, useSession } from 'next-auth/react'
import Title from "@/components/Layout/Title/Title";
import Loader from "@/components/Loader/Loader";

const LoginPage = () => {
	const session = useSession()
	const submitHandler = async (e: SyntheticEvent<HTMLFormElement>) => {
		e.preventDefault()

		const data: FormData = new FormData(e.currentTarget)
		const payload = {
			email: data.get('email'),
			password: data.get('password'),
		}

		const res = await signIn('credentials', {
			email: data.get('email'),
			password: data.get('password'),
		})

	}

	useEffect(() => {
		if(window == undefined) {
			 return
		}
	}, [])

	return (
		<>
			{session.status === "unauthenticated" && (
				<Form
					inputs={[
						{
							label: 'email',
							name: 'email',
							type: 'text',
							placeholder: 'admin@mail.ru',
						},
						{
							label: 'password',
							name: 'password',
							type: 'password',
						},
					]}
					submitHandler={submitHandler}
					title="Вход"
					buttonText="Войти"
				/>
			)}
			{session.status === "authenticated" && (
				<Title h={6}>You just login in</Title>
			)}
			{session.status === "loading" && (
				<Loader/>
			)}
		</>
	)
}

export default LoginPage
