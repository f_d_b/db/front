'use client'
import Title from '@/components/Layout/Title/Title'
import axios from 'axios'
import { useEffect, useState } from 'react'
import Loader from '@/components/Loader/Loader'
import Link from 'next/link'
import apiClient from "@/configs/axios";

interface Track {
	id: number
	coords: number[][]
	date: string
}

const TrackList = () => {
	const [trackList, setTrackList] = useState<Track[] | null>(null)
	const [isLoading, setIsLoading] = useState(true)

	useEffect(() => {
		const fetchTrackList = async () => {
			try {
				const response = await apiClient.get<Track[]>(
					'/api/v1/route/',
				)
				setTrackList(response.data)
				setIsLoading(false)
			} catch (error) {
				setIsLoading(false)
				console.error('Ошибка при выполнении GET-запроса:', error)
			}
		}

		fetchTrackList()
	}, [])

	return (
		<>
			<Title>Список маршрутов</Title>

			{isLoading && <Loader />}

			<div>
				{trackList && (
					<ul>
						{trackList.map((item: Track) => (
							<li key={item.id} style={{marginTop: '20px', listStyleType: 'none'}}>
								<Link href={`/defects/${item.id}`}>
									{item.id} - [start: {item.coords[0][0]},{item.coords[0][1]} |
									end: {item.coords[item.coords.length - 1][0]},
									{item.coords[item.coords.length - 1][1]}] -- {item.date}
								</Link>
							</li>
						))}
					</ul>
				)}
			</div>
		</>
	)
}

export default TrackList
