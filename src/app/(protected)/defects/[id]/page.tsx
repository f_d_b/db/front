'use client'
import Title from "@/components/Layout/Title/Title";
import {FullscreenControl, Map, Placemark, Polyline, YMaps} from "@pbe/react-yandex-maps";
import {Fragment, useEffect, useState} from "react";
import {usePathname} from "next/navigation";
import apiClient from "@/configs/axios";

interface Deffect {
    id: number
    is_actual: boolean
    coords: number[]
    photo?: string
    type: string
    date: string
    route_id: number
}

interface Track {
    id: number
    coords: number[][]
    date: string
    defects: Deffect[]
}

const getType = (type: string): string => {
    switch (type) {
        case 'pit':
            return 'Яма'
        case 'crack':
            return 'Трещина'
        default:
            return 'Дефект'
    }
}

const TrackDetailPage = () => {
    const [defects, setDefects] = useState<Deffect[] | null>(null)
    const [track, setTrack] = useState<Track | null>(null)
    const [isLoading, setIsLoading] = useState(true)

    const path = usePathname()
    const pahtParts = path.split('/')
    const id = pahtParts[pahtParts.length - 1]

    useEffect(() => {
        const fetchTrackList = async () => {
            try {
                const track = await apiClient.get<Track>(
                    `/api/v1/route/${id}`,
                )
                track.data.defects.forEach((deffect) => {
                    apiClient.get(`api/v1/route/get_photo?file_path=${deffect.photo}`)
                })
                setTrack(track.data)
                setIsLoading(false)
            } catch (error) {
                setIsLoading(false)
                console.error('Ошибка при выполнении GET-запроса:', error)
            }
        }

        let delay: number = 10000;
        let timeout = setInterval(fetchTrackList, delay);
        fetchTrackList()

        return () => timeout && clearInterval(timeout)
    }, [])

    return (
        <>
            <Title>Трек {track?.date} -- [start:{' '}
                {track?.coords[0][0]},{track?.coords[0][1]} | end:{' '}
                {track?.coords[track?.coords.length - 1][0]},
                {track?.coords[track?.coords.length - 1][1]}]</Title>
            <div style={{ marginTop: '20px' }}>
                <YMaps>
                    <Map
                        defaultState={{ center: [55.75, 37.57], zoom: 12 }}
                        width={'90%'}
                        height={620}
                    >
                        <FullscreenControl />
                        <Polyline
                            geometry={track?.coords}
                            options={{
                                strokeColor: "#000",
                                strokeWidth: 3,
                                strokeOpacity: 0.5,
                            }}
                        />
                        {track?.defects?.map((defect: Deffect) => {
                            return <Placemark key={`${defect.coords[0]}_${defect.coords[0]}`}
                                geometry={defect.coords}
                                properties={{
                                    balloonContentHeader: `Координаты: ${defect.coords[0]} | ${defect.coords[1]}`,
                                    balloonContentBody: `<img src='${defect.photo}'/>` ,
                                    balloonContentFooter: `${getType(defect.type)}`
                                }}
                                modules={["geoObject.addon.balloon"]}
                            />
                        })}

                    </Map>
                </YMaps>
            </div>
        </>
    )
}

export default TrackDetailPage