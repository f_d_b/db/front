'use client'
import Title from '@/components/Layout/Title/Title'
import Button from '@/components/Button/Button'
import Loader from '@/components/Loader/Loader'
import Text from '@/components/Layout/Text/Text'
import { ChangeEvent, SyntheticEvent, useState } from 'react'
import axios from 'axios'
import {Simulate} from "react-dom/test-utils";
import input = Simulate.input;
import apiClient from "@/configs/axios";

interface testImage {
	path?: string
}

const PhotoDetectListPage = () => {
	const [uploadedFiles, setUploadedFiles] = useState<FileList | null>(null)
	const [isLoading, setIsLoading] = useState(false)
	const [testImage, setTestImage] = useState<string>()
	const [error, setError] = useState()

	const handleUploadFiles = (e: ChangeEvent<HTMLInputElement>) => {
		setUploadedFiles(e.target.files)
		console.log('file uploaded')
	}

	const handleFileSubmit = async (e: SyntheticEvent) => {
		e.preventDefault()
		console.log('file submited')

		if (!uploadedFiles) {
			return
		}

		const formData = new FormData()
		formData.append(`file`, uploadedFiles[0])

		try {
			const res = await apiClient.postForm(
				'api/v1/defect/predict', formData)
			if (res.data.path) {
				await apiClient.get(`api/v1/route/get_photo?file_path=${res.data.path}`)
			}
			setTestImage(res.data.path)
			setError(undefined)
		} catch (e: any) {
			setError(e.message)
		}

	}

	return (
		<>
			<Title>Фотодетекции</Title>

			<form onSubmit={handleFileSubmit} className="form">
				{isLoading && <Loader />}
				{!isLoading && (
					<>
						<label htmlFor="photos">Добавить фотографии</label>
						<input
							onChange={handleUploadFiles}
							type="file"
							multiple
							name="name"
							className={'inputFile'}
						/>
					</>
				)}
				<Button disabled={isLoading}>Отправить</Button>
			</form>

			{testImage && <img src={testImage} />}
			{error && <Text>{error}</Text>}
		</>
	)
}

export default PhotoDetectListPage
