import { ReactNode } from 'react'
import {useSession} from "next-auth/react";
import {useRouter} from "next/router";
import {getServerSession} from "next-auth";
import {authConfig} from "@/configs/auth";
import {redirect} from "next/navigation";

const ProtectedLayout = async ({ children }: { children: ReactNode }) => {
	const session = await getServerSession(authConfig)
	if (!session) {
		// redirect('/login')
	}

	return (
		<>
			{ children }
		</>
	)
}

export default ProtectedLayout
