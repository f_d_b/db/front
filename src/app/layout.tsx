import './globals.scss'
import type { Metadata } from 'next'
import { Roboto } from 'next/font/google'
import { ReactNode } from 'react'
import Container from '@/components/Layout/Container/Container'
import { Providers } from '@/components/Providers'
import Debug from '@/components/Debug/Debug'
import Row from '@/components/Layout/Row/Row'
import Col from '@/components/Layout/Col/Col'
import Navbar from '@/components/Navbar/Navbar'

const roboto = Roboto({ weight: '400', subsets: ['cyrillic'] })

export const metadata: Metadata = {
	title: 'Roadar',
	description: 'DigitalUp Hackaton Project',
}

const RootLayout = ({ children }: { children: ReactNode }) => {
	return (
		<html lang="en">
			<body className={roboto.className}>
				<Providers>
					<Container full>
						<Row>
							<Col grow={1}>
								<Navbar />
							</Col>
							<Col grow={11}>{children}</Col>
						</Row>
					</Container>
				</Providers>
			</body>
		</html>
	)
}

export default RootLayout
