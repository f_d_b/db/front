'use client'
import { useSession } from 'next-auth/react'
import Title from "@/components/Layout/Title/Title";
import Text from "@/components/Layout/Text/Text";
import {redirect} from "next/navigation";

const Home = () => {
	const session = useSession()
	// if (session.status === 'unauthenticated') {
	// 	redirect('/login')
	// }

	return (
		<>
			<Title>Главная</Title>
			<Text>После сбора данных на этой странице планируется вывести статистику по различным видам деффектов.</Text>
		</>
	)
}

export default Home
