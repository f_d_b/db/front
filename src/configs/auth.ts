import { AuthOptions, User } from 'next-auth'
import Credentials from 'next-auth/providers/credentials'
import axios from "axios";

const users = [
	{
		email: 'test@mail.ru',
		password: 'password',
	},
]

async function refreshAccessToken(tokenObject: any) {
	try {
		// Get a new set of tokens with a refreshToken
		const tokenResponse = await axios.post(process.env.API_HOST + 'auth/refreshToken', {
			token: tokenObject.refreshToken
		});

		return {
			...tokenObject,
			accessToken: tokenResponse.data.accessToken,
			accessTokenExpiry: tokenResponse.data.accessTokenExpiry,
			refreshToken: tokenResponse.data.refreshToken
		}
	} catch (error) {
		return {
			...tokenObject,
			error: "RefreshAccessTokenError",
		}
	}
}

export const authConfig: AuthOptions = {
	providers: [
		Credentials({
			credentials: {
				email: { label: 'email', type: 'email', required: true },
				password: { label: 'password', type: 'password', required: true },
			},
			// @ts-ignore
			async authorize(credentials) {
				if (!credentials?.email || !credentials.password) return null

				console.log('try authorize')

				try {
					console.debug(credentials?.email, credentials.password)

					// Authenticate user with credentials
					const user = await axios.post(process.env.API_HOST + '/auth/login', {
						password: credentials.password,
						email: credentials.email
					});

					console.log(user.data.access_token)


					if (user.data.access_token) {
						console.debug(user.data.access_token)
						return user;
					}

					return null;
				} catch (e: any) {
					console.error(`failed auth!, ${e.message}`)
					throw new Error(e.message);
				}
			},
		}),
	],
	pages: {
		signIn: '/login',
	},
	callbacks: {
		jwt: async ({ token, user}: any) => {
			if (user) {
				// This will only be executed at login. Each next invocation will skip this part.
				token.accessToken = user.data.access_token;
				token.accessTokenExpiry = user.data.access_token_expiry;
				token.refreshToken = user.data.refresh_token;
			}

			// If accessTokenExpiry is 24 hours, we have to refresh token before 24 hours pass.
			const shouldRefreshTime = Math.round((token.accessTokenExpiry - 60 * 60 * 1000) - Date.now());

			// If the token is still valid, just return it.
			if (shouldRefreshTime > 0) {
				return Promise.resolve(token);
			}

			// If the call arrives after 23 hours have passed, we allow to refresh the token.
			token = refreshAccessToken(token);
			return Promise.resolve(token);
		},
		session: async ({ session, token }: any) => {
			// Here we pass accessToken to the client to be used in authentication with your API
			session.accessToken = token.accessToken;
			session.accessTokenExpiry = token.accessTokenExpiry;
			session.error = token.error;

			return Promise.resolve(session);
		},
	},
	secret: process.env.NEXTAUTH_SECRET
}
