module.exports = {
	semi: false,
	// tabWidth: 4,
	useTabs: true,
	singleQuote: true,
	trailingComma: 'all',
	jsxBracketSameLine: true,
}
